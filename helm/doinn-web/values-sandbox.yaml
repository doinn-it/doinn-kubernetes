namespace: doinn-sandbox

envFile: doinn-web-env-sandbox.env
# the number of replicas for the phpfpm-nginx pods
replicaCount: 1
replicaCountHorizon: 1

#Uncomment to add your image pull secret which has access to both PHP and nginx repositories
imagePullSecrets: google-container-registry-eu

# Set this to true to run an initial dumping and seeding of database on initial helm install. This does not seed on upgrades.
preInstallJob: false
preInstallCommand:
  # Start artisan in backgrounde mode, then dump database and seed.
  # The .env for the pre-install job is different because it needs to seed local files without SSL.
  # The APP_ENV is set to APP_ENV=seeder and the APP_URL is set to: APP_URL=http://localhost:8000
  # which aren't yet available over https at desired url.
  # If you change the below commands, please double check the [args] command in kubernetes/helm/laravel5/templates/deployment-phpfpm.yaml still works
  # Also check that the entrypoint.sh logic still works: docker/php-fpm/entrypoint.sh
  first: "php /doinn-web/artisan serve &"

# Set this to true to run an migrations before every upgrade
preUpgradeJob: false
preUpgradeCommand:
  # Preform migrations. If you change the below commands, please double check the [args] command in kubernetes/helm/doinn-web/templates/deployment-phpfpm.yaml still works
  # Also check that the entrypoint.sh logic still works: docker/php-fpm/entrypoint.sh
  first: "php artisan migrate --force"

# The nginx image repository details
nginxImage:
  repository: "eu.gcr.io/turnkey-skill-93714/doinn-web-nginx"
  tag: sandbox
  pullPolicy: Always

# The nginx readiness and liveness values
nginxLivenessProbe:
  initialDelaySeconds: 60
  timeoutSeconds: 5
  httpGetPath: "/admin"
nginxReadinessProbe:
  initialDelaySeconds: 60
  timeoutSeconds: 5
  httpGetPath: "/admin"  

# The phpfpm image repository details
phpfpmImage:
  repository: "eu.gcr.io/turnkey-skill-93714/doinn-web-phpfpm"
  tag: sandbox
  pullPolicy: Always

# The phpfpm readiness and liveness values
phpfpmLivenessProbe:
  initialDelaySeconds: 40
  timeoutSeconds: 5
phpfpmReadinessProbe:
  initialDelaySeconds: 60
  timeoutSeconds: 5

# The nginx service values
nginxService:
  type: NodePort
  externalPort: 80
  internalPort: 80

# The phpfpm port in the pod
phpfpmPort: 9000

# Ingress details
ingress:
  enabled: true
  annotations:
    # This tells the certificate cluster issuer
    certmanager.k8s.io/cluster-issuer: letsencrypt-prod
    # Pointing out a specific Ingress controller type (gce or nginx)
    kubernetes.io/ingress.class: gce
    # This tells which reserved global ip to use 
    kubernetes.io/ingress.global-static-ip-name: doinn-doinn-web-sandbox-external-ip
    # This tells the cert-manager cluster-issuer resource to create a certificate for this ingress
    kubernetes.io/tls-acme: "true"
    # This ensures that the same backend laravel pod gets the requests during a session. https://github.com/kubernetes/ingress-nginx/tree/master/docs/examples/affinity/cookie
    nginx.ingress.kubernetes.io/affinity: "cookie"
    nginx.ingress.kubernetes.io/session-cookie-name: "route"
    nginx.ingress.kubernetes.io/session-cookie-hash: "sha1"
  path: /*
  hosts:
    web:
      url: sandbox.doinn.co
      service: doinn-web-sandbox-nginx
    vendor:
      url: 
      service: doinn-vendor-sandbox-nginx
    host:
      url: 
      service: doinn-host-sandbox-nginx
    admin:
      url: 
      service: doinn-admin-sandbox-nginx

googleCloudStackdriver:
  credentialName: gcp-sandbox-stackdriver-service-account

googleCloudStorage:
  credentialName: gcp-sandbox-storage-service-account

googleCloudFirebase:
  credentialName: gcp-sandbox-firebase-service-account

nginxResources: {}
  # We usually recommend not to specify default resources and to leave this as a conscious
  # choice for the user. This also increases chances charts run on environments with little
  # resources, such as Minikube. If you do want to specify resources, uncomment the following
  # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
  # limits:
  #  cpu: 100m
  #  memory: 128Mi
  # requests:
  #  cpu: 100m
  #  memory: 128Mi

phpfpmResources: {}
  # We usually recommend not to specify default resources and to leave this as a conscious
  # choice for the user. This also increases chances charts run on environments with little
  # resources, such as Minikube. If you do want to specify resources, uncomment the following
  # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
  # limits:
  #  cpu: 100m
  #  memory: 128Mi
  # requests:
  #  cpu: 100m
  #  memory: 128Mi

nodeSelector: {}

tolerations: []

affinity: {}
