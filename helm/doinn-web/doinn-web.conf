server {

    listen 80;
    listen [::]:80;

    server_name _;
    if ($http_x_forwarded_proto = "http") {
        return 301 https://$host$request_uri;
    }

    add_header Access-Control-Allow-Origin * always;
    add_header Access-Control-Allow-Headers 'Accept, Authorization, Content-Type, Origin, X-Requested-With, api-key, scope, X-DOINN-LoggedOriginalUser';
    add_header Access-Control-Allow-Methods 'OPTIONS, GET, POST, PUT, PATCH, DELETE';
    add_header X-Frame-Options "ALLOW-FROM https://*.doinn.co";
    add_header Content-Security-Policy "frame-ancestors 'self' https://*.doinn.co";

    root /doinn-web;
    index index.php server.php index.html;

    rewrite ^/auth/(.*)$ /en_US/auth/$1 permanent;
    rewrite ^/password/(.*)$ /en_US/password/$1 permanent;

    ## Disable .htaccess, .git and other hidden files access
    location ~ /\.(?!well-known).*/ {
        return 404;
        deny all;
        access_log off;
        log_not_found off;
    }

    location ~* .(txt|ico)$ {
        try_files $uri /public/$uri;
    }

    location / {
        index /public/index.php;
        try_files $uri $uri/ /public/index.php$is_args$args;
    }

    location ~* "^(\/public\/cdn)\/[a-f0-9]{32}(\/*\.*(.*))$" {
        try_files $uri $1$2;
    }

    location /vendor/horizon {
        try_files $uri /public/$uri;
    }

    location ~ \.php$ {
        try_files $uri /index.php =404;
        fastcgi_pass localhost:9000;
        fastcgi_index index.php;
        fastcgi_buffers 16 16k;
        fastcgi_buffer_size 32k;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        #fixes timeouts
        fastcgi_read_timeout 600;
        include fastcgi_params;
    }
    
    location ~ /\.ht {
        deny all;
    }

    error_log /var/log/nginx/error.log;
    access_log /var/log/nginx/access.log;
}